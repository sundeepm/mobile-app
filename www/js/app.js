angular.module('mApp', ['ionic', 'mApp.controllers', 'ngCordova','ionic.utils'])

    .run(function ($ionicPlatform, $rootScope, $timeout,$localstorage,$state,$location,$ionicLoading,$window) {
	
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });
        $rootScope.$on('loading:show', function() {
            $ionicLoading.show({
                template: 'loading...'
            })
        })

        $rootScope.$on('loading:hide', function() {
            $ionicLoading.hide()
        })
          
        $rootScope.logout=function(){
            $localstorage.setObject('EmrixUser','');
            $rootScope.EmrixUser={};
            $state.go("app.Login",{}, {
                reload: true
            });
        } 
        if(angular.isUndefined($localstorage.getObject('EmrixUser')) || $localstorage.getObject('EmrixUser')=='' ){
            $state.go("app.Login",{}, {
                reload: true
            });

        }else{
            $rootScope.EmrixUser=$localstorage.getObject('EmrixUser')
            console.log("username: "+$rootScope.EmrixUser.username);
            $state.go("app.Centres",{}, {
                reload: true
            });
        }
        
    })

    .config(function ($provide, $stateProvider, $urlRouterProvider,$httpProvider,$ionicConfigProvider) {
        $ionicConfigProvider.backButton.previousTitleText(false);	
        $stateProvider
        .state('app', {
            url: "/app",
            abstract: true,
            templateUrl: "templates/menu.html",
            controller: 'AppCtrl'
        })

        .state('app.Home', {
            url: "/Home",
            views: {
                'menuContent': {
                    templateUrl: "templates/Home.html",
                    controller: "HomeCtrl"
                }
            }
        })
        .state('app.Login', {
            url: "/Login",
            views: {
                'menuContent': {
                    templateUrl: "templates/Login.html",
                    controller: "LoginController"
                }
            }
        })
        .state('app.Centres', {
            url: "/Centres",
            views: {
                'menuContent': {
                    templateUrl: "templates/Centres.html",
                    controller:"CentresCtrl"
                }
            }
        })
        .state('app.Iframe', {
            url: "/Iframe",
            params:{
                'url': null
            },
            views: {
                'menuContent': {
                    templateUrl: "templates/Iframe.html",
                    controller:"IframeCtrl"
                }
            }
        })
        ;
        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/Centres');
    });
