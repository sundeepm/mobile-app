angular.module('mApp.controllers', ["ionic","ngCordova","ionic.utils",'ngMessages','ngSanitize'])
    .factory('AuthenticationService',
        ['$http', '$rootScope','$localstorage',
        function ($http, $rootScope, $localstorage) {
            var serviceBase = "http://iryx.vidoc.in:8080/users/auth.php";
            var service = {};
            service.Login = function (user, callback) {
                var data = $.param({
                    username: user.username,
                    password: user.password
                });
                var config = {
                    headers : {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                }
                $http.post(serviceBase, data, config)
                .success(function (response) {
                    console.log(response);
                    callback(response);
                });
            }       
            service.SetCredentials = function (user) {
                //var authdata = Base64.encode(email + ':' + password);
                var EmrixUser= {
                    username: user.username,
                    password:user.password
                };
                $localstorage.setObject("EmrixUser",EmrixUser);
                $rootScope.EmrixUser = EmrixUser; 
            };
            service.ClearCredentials = function () {
                $rootScope.EmrixUser = {};
                $localstorage.setObject("EmrixUser",'');
            };
            return service;
        }])

    .controller('LoginController', function ($scope,AuthenticationService,$ionicLoading,$state,$ionicModal) {
        // reset login status
        //AuthenticationService.ClearCredentials();
        var loginData = this.loginData;
        var loginData=$scope.loginData;
        $scope.login = function (loginData) {
            console.log(loginData);
            $ionicLoading.show({
                template: 'Please wait while we login...'
            }); 
            AuthenticationService.Login(loginData, function (response) {
                $ionicLoading.hide()
                if (angular.isDefined(response.user_hash)) {
                    AuthenticationService.SetCredentials(loginData);
                    $state.go('app.Centres');
                } else {
                    $scope.loginData={};
                    alert("Oops.. did you check your email and password??try again");
                    $state.go('app.Login');
                }
            });
        };
    })

    .controller('AppCtrl', function ($scope, $ionicModal, $timeout, $rootScope,$localstorage,$state) {

    })

    .controller("HomeCtrl", function ($scope, $rootScope, $cordovaLocalNotification, $state) {
        $scope.a=[1,2,3,4];
        $scope.images=[];
    
        $scope.loadImages = function() {
            for(var i = 0; i < 10; i++) {
                $scope.images.push({
                    id: i, 
                    src: "http://placehold.it/50x50"
                });
            }
        }
        $scope.loadImages();
    })
    
    .controller("IframeCtrl", function ($scope,$localstorage,$sce) {
        var ExmrixUser=$localstorage.getObject('EmrixUser');
        //$scope.centreUrl=$stateParams.url+"auth.php?username="+ExmrixUser.username+"&password="+ExmrixUser.password
        $scope.centreUrl= $sce.trustAsResourceUrl('http://emrbeta.vidoc.in/index.php?action=index&module=Home');
        console.log($scope.centreUrl);
    })

    .service("CentresService", function ($http) {
        
        //get centres list
        var serviceBase = "http://iryx.vidoc.in:8080/users/center.php?username=tejas.shroff&password=tejas123";
        this.getCentres = function (callback) {
            $http.get(serviceBase)
            .then(function (response) {
                callback(response);
            });
        
        };
        
        //login for centreslist
        this.Login = function (url,user, callback) {
            url=url+'/auth.php';
            var data = $.param({
                username: user.username,
                password: user.password
            });
            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }
            $http.post(url, data, config)
            .success(function (response) {
                console.log(response);
                callback(response);
            });
        }
        
    })
    
    

    .controller("CentresCtrl", function ($scope,$http,$localstorage,CentresService,$state,$rootScope) {
        //check if user is logged in or not
        if(angular.isUndefined($localstorage.getObject('EmrixUser')) || $localstorage.getObject('EmrixUser')=='' ){
            $state.go("app.Login",{}, {
                reload: true
            });
        }
        //get all centres for user 
        if(angular.isDefined($localstorage.getObject('EmrixUser'))){
            var EmrixUser=$localstorage.getObject('EmrixUser')
            var username=EmrixUser.username
            var password=EmrixUser.password
        }
        var serviceBase = "http://iryx.vidoc.in:8080/users/center.php?username="+username+"&password="+password;
        //$scope.Centres=[];
        $http({
            method: 'GET',
            url: serviceBase
        }).
        success(function(data, status, header, config) {
                var data=[  {"id":"1","center_id":"25616","centername":"Lilavati Hospital","centertype":"Hospital","directorname":"Shri Prabodh Kirtilal Mehta","dateofincorporation":"1978-04-24","numberofemployees":"700","numberOfbeds":"2000",
                            "corporateid":"COP-1","regno":"789","isactive":"1","mrnschemebasevalue":"LHRC","mrnschemeoffset":"1",
                            "imageurl":"http:\/\/emrbeta.vidoc.in\/storage\/2015\/October\/week4\/110776_nanavati-hospital.jpg","centernotes":null,"createdby":null,"modifiedby":null,"dateifincorporation":null,"icd10applicable":null,"icd10procapplicable":null,"app_url":"http://emrbeta.vidoc.in/"},
                            {"id":"2","center_id":"19","centername":"HN Hospital","centertype":"Hospital","directorname":"Dr.Gaurang Shah","dateofincorporation":"1978-12-12","numberofemployees":"500","numberOfbeds":"1800","corporateid":"","regno":"","isactive":"1","mrnschemebasevalue":"15-HN","mrnschemeoffset":"","imageurl":"http://emrbeta.vidoc.in/storage/2015/October/week4/110774_nanavati-hospital.jpg","centernotes":null,"createdby":null,"modifiedby":null,"dateifincorporation":null,"icd10applicable":null,"icd10procapplicable":null,"app_url":"http://emrbeta.vidoc.in/"},{"id":"1","center_id":"25616","centername":"Lilavati Hospital","centertype":"Hospital","directorname":"Shri Prabodh Kirtilal Mehta","dateofincorporation":"1978-04-24","numberofemployees":"700","numberOfbeds":"2000",
                            "corporateid":"COP-1","regno":"789","isactive":"1","mrnschemebasevalue":"LHRC","mrnschemeoffset":"1",
                            "imageurl":"http:\/\/emrbeta.vidoc.in\/storage\/2015\/October\/week4\/110776_nanavati-hospital.jpg","centernotes":null,"createdby":null,"modifiedby":null,"dateifincorporation":null,"icd10applicable":null,"icd10procapplicable":null,"app_url":"http://emrbeta.vidoc.in/"},
                            {"id":"2","center_id":"19","centername":"HN Hospital","centertype":"Hospital","directorname":"Dr.Gaurang Shah","dateofincorporation":"1978-12-12","numberofemployees":"500","numberOfbeds":"1800","corporateid":"","regno":"","isactive":"1","mrnschemebasevalue":"15-HN","mrnschemeoffset":"","imageurl":"http://emrbeta.vidoc.in/storage/2015/October/week4/110774_nanavati-hospital.jpg","centernotes":null,"createdby":null,"modifiedby":null,"dateifincorporation":null,"icd10applicable":null,"icd10procapplicable":null,"app_url":"http://emrbeta.vidoc.in/"}]
              $scope.Centres= data;
        
            console.log($scope.Centres);
        }).error(function(data, status, header, config){
            console.log("error while fetching CentreList");                
        })
        
        $scope.ReDirectToCentre=function(baseUrl){
            console.log(baseUrl);
            var EmrixUser=$localstorage.getObject('EmrixUser');
            $state.go('app.Iframe',{
                'url' : baseUrl
            });
        }
        $scope.color="";
        $scope.GenerateRandomColor=function(){
            $scope.color='#'+Math.floor(Math.random()*16777215).toString(16);
            console.log($scope.color);
        }
//        $scope.SetRandomBG=function(){
//            $scope.GenerateRandomColor();
//            console.log("color"+$scope.color);
//            return $scope.color;
//        }

        $scope.images=[];
    
        $scope.loadImages = function() {
            for(var i = 0; i < 10; i++) {
                $scope.images.push({
                    id: i, 
                    src: "http://placehold.it/50x50"
                });
            }
        }
        $scope.loadImages();
    
    });




